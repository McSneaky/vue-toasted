# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

# [1.3.0](https://gitlab.com/gitlab-org/frontend/vue-toasted/compare/v1.2.1...v1.3.0) (2019-11-13)


### Features

* Add callback support to action icons ([d530400](https://github.com/shakee93/vue-toasted/commit/d530400))



## [1.2.1](https://gitlab.com/gitlab-org/frontend/vue-toasted/compare/v1.2.0...v1.2.1) (2019-04-26)


### Bug Fixes

* Revert "added padding to support multiline fixed" ([d6ebcf4](https://gitlab.com/gitlab-org/frontend/vue-toasted/commit/d6ebcf4))



# [1.2.0](https://github.com/shakee93/vue-toasted/compare/v1.1.26...v1.2.0) (2019-04-26)


### Features

* Add keepOnHover option ([0aee055](https://github.com/shakee93/vue-toasted/commit/0aee055))
